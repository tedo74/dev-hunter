import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Developer } from 'src/app/models/dev.model';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { Tech } from 'src/app/models/tech.model';

@Component({
  selector: 'dev-dev-full-info',
  templateUrl: './dev-full-info.component.html',
  styleUrls: ['./dev-full-info.component.scss'],
})
export class DevFullInfoComponent implements OnInit {
  developer: Developer;
  location: DevLocation;
  tech: Tech;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    data: { dev: Developer; loc: DevLocation; tech: Tech },
    private dialogRef: MatDialogRef<DevFullInfoComponent>
  ) {
    this.developer = data.dev;
    this.location = data.loc;
    this.tech = data.tech;
  }

  ngOnInit(): void {}

  close() {
    this.dialogRef.close();
  }
}
