import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { HunterService } from 'src/app/main/hunter/hunter.service';
import { Developer } from 'src/app/models/dev.model';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { Tech } from 'src/app/models/tech.model';
import { DevFullInfoComponent } from '../dev-full-info/dev-full-info.component';

@Component({
  selector: 'dev-dev-info',
  templateUrl: './dev-info.component.html',
  styleUrls: ['./dev-info.component.scss'],
})
export class DevInfoComponent implements OnInit {
  @Input() developer!: Developer;
  location!: DevLocation;
  tech!: Tech;
  constructor(
    private hunterService: HunterService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.hunterService
      .getLocation$(this.developer.location)
      .subscribe((loc) => (this.location = loc));
    this.hunterService.getTech$(this.developer.technology).subscribe((tech) => {
      this.tech = tech;
    });
  }

  openInfo(dev: Developer) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    let loc = this.location;
    let tech = this.tech;
    dialogConfig.data = { dev, loc, tech };
    const dialog = this.dialog.open(DevFullInfoComponent, dialogConfig);
  }
}
