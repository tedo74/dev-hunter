import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { HunterService } from 'src/app/main/hunter/hunter.service';
import { Developer } from 'src/app/models/dev.model';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { Tech } from 'src/app/models/tech.model';

@Component({
  selector: 'dev-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @Output() filterItem = new EventEmitter<{
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  }>();

  technologies: Tech[] = [];
  locations: DevLocation[] = [];

  // colect all subscriptions to unsubscribe;
  subscriptions: Subscription[] = [];
  selectedTech = 'all';
  selectedLoc = 'all';

  // for filter and search
  locationId = 0;
  techId = 0;
  searchTerm = '';
  sortDirection = '';

  searchForm: FormGroup;

  constructor(private hunterService: HunterService) {
    this.loadTechs();
    this.loadLocations();

    this.searchForm = new FormGroup({
      search: new FormControl(''),
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  // emit selected tech id to filter devs in parent component
  onChangeTech(id: number) {
    // this.loadTechs();
    if (id) {
      this.techId = id;
    } else {
      this.techId = 0;
    }
    this.filterItem.emit({
      locationId: this.locationId,
      techId: this.techId,
      searchTerm: this.searchTerm,
      sortDirection: this.sortDirection,
    });
  }
  onChangeLoc(id: number) {
    if (id) {
      this.locationId = id;
    } else {
      this.locationId = 0;
    }
    this.filterItem.emit({
      locationId: this.locationId,
      techId: this.techId,
      searchTerm: this.searchTerm,
      sortDirection: this.sortDirection,
    });
  }

  onSearch() {
    this.searchTerm = this.searchForm.value.search;
    // if (this.searchTerm) {
    this.filterItem.emit({
      locationId: this.locationId,
      techId: this.techId,
      searchTerm: this.searchTerm,
      sortDirection: this.sortDirection,
    });
    // }
  }

  sortPrice(up: boolean) {
    if (up) {
      this.sortDirection = 'asc';
    } else {
      this.sortDirection = 'desc';
    }
    this.filterItem.emit({
      locationId: this.locationId,
      techId: this.techId,
      searchTerm: this.searchTerm,
      sortDirection: this.sortDirection,
    });
    this.sortDirection = '';
  }

  loadTechs() {
    let techSubscr = this.hunterService.getAllTechs$().subscribe({
      next: (techs) => {
        this.technologies = techs;
      },
    });
    this.subscriptions.push(techSubscr);
  }

  loadLocations() {
    let locSubscr = this.hunterService.getAllLocations$().subscribe({
      next: (locs) => {
        this.locations = locs;
      },
    });
    this.subscriptions.push(locSubscr);
  }
}
