import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DevInfoComponent } from './dev-info/dev-info.component';
import { MaterialModule } from '../material/material.module';
import { DevFullInfoComponent } from './dev-full-info/dev-full-info.component';
import { SearchComponent } from './search/search.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [DevInfoComponent, DevFullInfoComponent, SearchComponent],
  imports: [CommonModule, MaterialModule, ReactiveFormsModule],
  exports: [DevInfoComponent, DevFullInfoComponent, SearchComponent],
})
export class SharedModule {}
