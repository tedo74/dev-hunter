import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth-guard';
import { AboutComponent } from './main/Site-parts/about/about.component';
import { LandingPageComponent } from './main/Site-parts/landing-page/landing-page.component';
import { NotFoundComponent } from './main/Site-parts/not-found/not-found.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: LandingPageComponent },
  { path: 'about', component: AboutComponent },
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: 'devs',
    loadChildren: () =>
      import('./main/dev/dev.module').then((m) => m.DevModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'hunter',
    loadChildren: () =>
      import('./main/hunter/hunter.module').then((m) => m.HunterModule),
    canLoad: [AuthGuard],
  },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
