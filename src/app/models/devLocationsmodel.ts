export interface DevLocation {
  id: number;
  location: string;
  link?: string;
  used: number;
  removable: boolean;
}
