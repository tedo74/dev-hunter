export interface Developer {
  id: number;
  name: string;
  email: string;
  phone: number;
  location: number;
  technology: number;
  pricePerHour: number;
  yearsExperience: number;
  language: string[];
  pictureUrl?: string;
  description?: string;
  linkedinLink?: string;

  hiredDates: { startDate: Date; endDate: Date }[];
  isHired: boolean;
  selectedForJob?: boolean;
  time: Date;
}
