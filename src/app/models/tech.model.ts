export interface Tech {
  id: number;
  techName: string;
  imageUrl?: string;
  used: number;
  removable: boolean;
}
