export interface Hunter {
  // developer id
  id: number;

  // job start and end dates
  dates: { startDate: Date; endDate: Date };
}
