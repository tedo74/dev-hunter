import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'dev-register',
  templateUrl: './register.component.html',
  styleUrls: ['../auth.scss', './register.component.scss'],
})
export class RegisterComponent implements OnInit, OnDestroy {
  regErrorMessage = '';
  subscriptions: Subscription[] = [];
  constructor(private authService: AuthService, private router: Router) {}

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  ngOnInit(): void {}
  onRegisterHandler(data: NgForm) {
    let regSubscr = this.authService.register$(data).subscribe(
      (accessToken: string) => {
        this.authService.setToken(accessToken);
        this.authService.authChange.next(true);
        let userId = this.authService.getUserId();
        let createSubscr = this.authService.createHireArr(+userId).subscribe();
        this.subscriptions.push(createSubscr);
        this.router.navigate(['/devs']);
      },
      (err) => {
        console.log('HTTP Error', err);
        if (err.error === 'Email already exists') {
          this.regErrorMessage = 'Email already exists';
        } else {
          this.regErrorMessage = 'Server error';
        }
        this.authService.authChange.next(false);
      }
    );
    this.subscriptions.push(regSubscr);
  }
}
