import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable, Subject } from 'rxjs';
import { map, pluck } from 'rxjs/operators';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  authChange = new Subject<boolean>();
  helper = new JwtHelperService();
  // token = '';
  constructor(private httpClient: HttpClient, private router: Router) {}

  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getToken(): string {
    let token = localStorage.getItem('token') || '';
    try {
      if (!token || this.helper.isTokenExpired(token)) {
        throw new Error('getToken() error!');
        // this.router.navigate(['auth/login']);
      }
    } catch (error) {
      // console.log(error);
    }
    return token;
  }

  getName$(): Observable<string> {
    let id = this.getUserId();
    return this.httpClient
      .get<User>(`http://localhost:3000/users/${id}`)
      .pipe(pluck('name'));
  }

  isUserAuth(): boolean {
    try {
      let token = this.getToken();
      if (token && !this.helper.isTokenExpired(token)) {
        return true;
      }
    } catch (error) {
      console.log('is Auth token error');
      this.logOut();
      return false;
    }
    return false;
  }

  getUserId(): string {
    // return string id
    let id = '';
    if (this.isUserAuth()) {
      let token = this.getToken();
      try {
        id = this.helper.decodeToken(token).sub;
      } catch (error) {
        console.log('Auth service getUserId() token error');
        this.router.navigate(['/auth/login']);
      }
    }
    return id;
  }

  register$(data: NgForm): Observable<string> {
    let body = {
      email: data.value.email,
      password: data.value.pass,
      name: data.value.user,
    };

    return this.httpClient
      .post<{ accessToken: string }>(`http://localhost:3000/register`, body)
      .pipe(map((data) => data.accessToken));
  }

  login$(data: NgForm): Observable<string> {
    let body = {
      email: data.value.email,
      password: data.value.pass,
    };

    return this.httpClient
      .post<{ accessToken: string }>(`http://localhost:3000/login`, body)
      .pipe(map((data) => data.accessToken));
  }

  logOut(): void {
    localStorage.removeItem('token');
    this.authChange.next(false);
    this.router.navigate(['/auth/login']);
    location.reload();
  }

  // On register create array for Hunter/User to save hired devs
  createHireArr(id: number) {
    let body = {
      userId: id,
      hired: [],
    };
    return this.httpClient.post(`http://localhost:3000/hunters`, body);
  }
}
