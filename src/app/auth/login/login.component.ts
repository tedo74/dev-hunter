import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'dev-login',
  templateUrl: './login.component.html',
  styleUrls: ['../auth.scss', './login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  loginErrorMessage = '';
  subscription: Subscription | undefined;
  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {}

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onLoginHandler(data: NgForm) {
    this.subscription = this.authService.login$(data).subscribe(
      (accessToken: string) => {
        this.authService.setToken(accessToken);
        this.authService.authChange.next(true);
        this.router.navigate(['/devs']);
      },
      (err) => {
        console.log('HTTP Error', err);
        if (
          err.error === 'Cannot find user' ||
          err.error === 'Incorrect password'
        ) {
          this.loginErrorMessage = 'Check email or password';
        } else {
          this.loginErrorMessage = 'Server error';
        }
        this.authService.authChange.next(false);
      }
    );
  }
}
