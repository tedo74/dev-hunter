import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavComponent } from './Site-parts/nav/nav.component';
import { LandingPageComponent } from './Site-parts/landing-page/landing-page.component';
import { AboutComponent } from './Site-parts/about/about.component';
import { RouterModule } from '@angular/router';
import { NotFoundComponent } from './Site-parts/not-found/not-found.component';
import { MaterialModule } from '../material/material.module';
import { LogoComponent } from './Site-parts/logo/logo.component';
import { FooterComponent } from './Site-parts/footer/footer.component';

@NgModule({
  declarations: [
    NavComponent,
    LandingPageComponent,
    AboutComponent,
    NotFoundComponent,
    LogoComponent,
    FooterComponent,
  ],
  imports: [CommonModule, RouterModule, MaterialModule],
  exports: [NavComponent, LandingPageComponent, FooterComponent],
})
export class MainModule {}
