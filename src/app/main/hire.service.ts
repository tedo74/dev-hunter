import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { AuthService } from '../auth/auth.service';
import { Hunter } from '../models/huter.model';

@Injectable({
  providedIn: 'root',
})
export class HireService {
  huntersUrl = `http://localhost:3000/hunters/`;
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {}

  getUserId() {
    return this.authService.getUserId();
  }

  // array of devs hired by current Hunter (Hunter=User)
  getHired$(): Observable<Hunter[]> {
    let id = this.getUserId();
    return this.httpClient
      .get<Hunter[]>(`${this.huntersUrl}${id}`)
      .pipe(pluck('hired'));
  }

  // update array of devs hired by current Hunter
  hireDev$(hiredArray: Hunter[]): Observable<Hunter[]> {
    let id = this.getUserId();
    let body = {
      hired: hiredArray,
    };
    return this.httpClient.patch<Hunter[]>(`${this.huntersUrl}${id}`, body);
  }
}
