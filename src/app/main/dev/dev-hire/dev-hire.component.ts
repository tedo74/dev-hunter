import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DevsService } from '../devs.service';

@Component({
  selector: 'dev-dev-hire',
  templateUrl: './dev-hire.component.html',
  styleUrls: ['./dev-hire.component.scss'],
})

// Sub component dates range and hire button
export class DevHireComponent implements OnInit {
  @Input() dates: { startDate: Date; endDate: Date }[] = [];
  @Output() submitEvent = new EventEmitter<{
    startDate: Date;
    endDate: Date;
  }>();
  minDate = new Date();

  constructor(private devService: DevsService) {}

  ngOnInit(): void {
    this.minDate = new Date();
  }

  myDateFilter = (d: Date | null): boolean => {
    const calDate = d || new Date();
    return this.devService.dateFilter(this.dates, calDate);
  };

  submitHire(f: NgForm) {
    let newDates = { startDate: f.value.start, endDate: f.value.end };
    this.submitEvent.emit(newDates);
    f.resetForm();
  }
}
