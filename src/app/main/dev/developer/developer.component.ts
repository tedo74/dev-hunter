import { HttpErrorResponse } from '@angular/common/http';
import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Developer } from 'src/app/models/dev.model';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { Tech } from 'src/app/models/tech.model';
import { HunterService } from '../../hunter/hunter.service';
import { DevsService } from '../devs.service';

@Component({
  selector: 'dev-developer',
  templateUrl: './developer.component.html',
  styleUrls: ['./developer.component.scss'],
})
export class DeveloperComponent implements OnInit, OnDestroy {
  @Input() developer!: Developer;
  location!: DevLocation;
  tech!: Tech;

  // colect all subscriptions to unsubscribe;
  subscriptions: Subscription[] = [];

  @Output() deletedItem = new EventEmitter<number>();
  @Output() hireEvent = new EventEmitter<Developer>();

  // dev
  @Output() devSelectedForJob = new EventEmitter<{
    dev: Developer;
    tech: Tech;
  }>();
  // dev id
  @Output() devDeSelectedForJob = new EventEmitter<number>();

  // used for date filter
  minDate: Date = new Date();

  constructor(
    private devService: DevsService,
    private hunterService: HunterService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.minDate = new Date();
    // get current dev locatiion
    let loadLocationSub = this.hunterService
      .getLocation$(this.developer.location)
      .subscribe({
        next: (loc) => {
          this.location = loc;
        },
        error: (err) => {
          if ((err = 'Missing authorization header')) {
            this.router.navigate(['/auth/login']);
          } else {
            console.log(err);
          }
        },
      });

    let loadTechSub = this.hunterService
      // get current dev technology
      .getTech$(this.developer.technology)
      .subscribe({
        next: (tech) => {
          this.tech = tech;
        },
        error: (err) => {
          if ((err = 'Missing authorization header')) {
            this.router.navigate(['/auth/login']);
          } else {
            console.log(err);
          }
        },
      });

    this.subscriptions.push(loadLocationSub, loadTechSub);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  deleteDev(dev: Developer) {
    // clear old jobs and check if dev is hired or not to allow delete
    this.devService.clearDevOldJobs(dev);
    if (this.devService.checkDevHired(dev)) {
      return;
    }

    let deleteDevSub = this.devService.deleteDev$(dev.id).subscribe({
      next: () => {
        this.deletedItem.emit(dev.id);
        // location and tech subtract
        let locUsed = +this.location.used;
        let techUsed = +this.tech.used;
        locUsed--;
        techUsed--;
        let bodyLoc = { used: locUsed };
        let bodyTech = { used: techUsed };
        let updateLocSub = this.hunterService
          .updateLocation$(this.location.id, bodyLoc)
          .subscribe();
        let updateTechSub = this.hunterService
          .updateTech$(this.tech.id, bodyTech)
          .subscribe();
        this.subscriptions.push(updateLocSub, updateTechSub);
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(deleteDevSub);
  }

  // to edit component with dev id
  editDev(dev: Developer) {
    // this.router.navigate(['/devs/edit', dev.id]);
    this.router.navigate(['/devs/edit', dev.id]);
  }

  // to Parent component
  hireDev() {
    this.hireEvent.emit(this.developer);
  }

  // add new dates to developer
  submitDev(newDates: { startDate: Date; endDate: Date }) {
    // add new dates to current developer
    this.developer.hiredDates.push(newDates);
    // get all dates, old and this new dates
    let hiredDates = this.developer.hiredDates;
    // for update developer on DB
    let body = { hiredDates };

    let editDevSub = this.devService
      .editDev$(body, this.developer.id)
      .subscribe({
        next: () => {
          this.developer.isHired = true;
        },
        error: (err) => {
          if ((err = 'Missing authorization header')) {
            this.router.navigate(['/auth/login']);
          } else {
            console.log(err);
          }
        },
      });

    this.hireDev();

    this.subscriptions.push(editDevSub);
  }

  // developer select or deselct for teamwork
  toggle(event: MatCheckboxChange) {
    this.developer.selectedForJob = event.checked;
    if (this.developer.selectedForJob) {
      this.devSelectedForJob.emit({ dev: this.developer, tech: this.tech });
      let body = { selectedForJob: true };
      let editSubscr = this.devService
        .editDev$(body, this.developer.id)
        .subscribe();
      this.subscriptions.push(editSubscr);
    } else {
      this.devDeSelectedForJob.emit(this.developer.id);
      let body = { selectedForJob: false };
      let editSubscr = this.devService
        .editDev$(body, this.developer.id)
        .subscribe();
      this.subscriptions.push(editSubscr);
    }
  }
}
