import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AllDevsComponent } from './all-devs/all-devs.component';
import { DevsRoutingModule } from './devs.routing.module';
import { DeveloperComponent } from './developer/developer.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { DevHireComponent } from './dev-hire/dev-hire.component';
import { CreateEditComponent } from './create-edit/create-edit.component';

@NgModule({
  declarations: [
    AllDevsComponent,
    DeveloperComponent,
    DevHireComponent,
    CreateEditComponent,
  ],
  imports: [
    CommonModule,
    DevsRoutingModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
  ],
})
export class DevModule {}
