import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Developer } from 'src/app/models/dev.model';
import { Hunter } from 'src/app/models/huter.model';
import { Tech } from 'src/app/models/tech.model';
import { HireService } from '../../hire.service';
import { DevsService } from '../devs.service';

@Component({
  selector: 'dev-all-devs',
  templateUrl: './all-devs.component.html',
  styleUrls: ['./all-devs.component.scss'],
})
export class AllDevsComponent implements OnInit, OnDestroy {
  // for pagination
  @ViewChild('paginator') paginator!: MatPaginator;
  devsCount = 0;
  page = 1;
  limit = this.getLimit();

  // if limit is changed and saved in local storage;
  getLimit() {
    let limit = localStorage.getItem('limit');
    if (limit) {
      return +limit;
    }
    return 4;
  }

  // search / filter params from search component
  params: {
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  } | null = null;

  // MatPaginator Output
  pageEvent!: PageEvent;

  // all devs
  developers: Developer[] = [];

  // selected devs for team hire
  selectedDevelopers: { dev: Developer; tech: Tech }[] = [];

  //Not allow to hire for past period of time
  minDate = new Date();

  // all dates from selected to hire devs
  hiredDatesFromAllDevs: { startDate: Date; endDate: Date }[] = [];

  // hired developers from Hunter
  hired: Hunter[] = [];

  // collect subscription to unsubscribe
  subscriptions: Subscription[] = [];

  constructor(
    private devService: DevsService,
    private hireService: HireService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    // load all devs
    this.loadDevs(this.page, this.limit);
    // Array with hired devs from Hunter (Hunter = User)
    // evry hired is saved hire
    this.getHired();
    // limit page
    this.getLimit();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  // load all developers
  loadDevs(page: number, limit: number) {
    // *****old*****
    //
    // let loadAllDevsSubs = this.devService
    //   .getAll$()
    //   .subscribe((d) => (this.developers = d));

    // load all developers
    let loadAllDevsSubs = this.devService
      .getHeaderTest$(page, limit)
      .subscribe({
        next: (res) => {
          // for pagination
          let count = res.headers.get('X-Total-Count');
          if (count) {
            this.devsCount = +count;
          }
          this.developers = res.body;
          this.developers.forEach((d) => {
            // clear old job dates
            this.devService.clearDevOldJobs(d);
            // allow / not allow to delete dev
            if (this.devService.checkDevHired(d)) {
              d.isHired = true;
            } else {
              d.isHired = false;
            }
          });
        },
        error: (err) => {
          if ((err = 'Missing authorization header')) {
            this.router.navigate(['/auth/login']);
          } else {
            console.log(err);
          }
        },
      });
    this.subscriptions.push(loadAllDevsSubs);
  }

  createDev() {
    this.router.navigate(['/devs/create']);
  }

  // reload after delete developer in child component
  devDeleted(id: number) {
    this.loadDevs(this.page, this.limit);
    this.openSnackBar('developer deleted');
  }

  // return array to push hired for Hunter===User
  getHired() {
    let hiredSubs = this.hireService.getHired$().subscribe({
      next: (d) => {
        this.hired = d;
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(hiredSubs);
  }

  // push hired to array
  devHired(dev: Developer) {
    // get last dates
    let lastDates = dev.hiredDates[dev.hiredDates.length - 1];

    // push in hired array -- for Hunter
    // object from dates and dev id
    this.hired.push({ id: dev.id, dates: lastDates });
    // upload hunter array with new hired devs
    this.uploadHired();

    // delete from componetn arr (refresh view)
    this.developers = this.developers.filter((d) => d !== dev);

    // load dev to arr. Updated in DB from developer component (refresh view)
    this.developers.push(dev);
    this.resort();
  }

  //upload hired to Hunter array with devs on DB
  uploadHired() {
    let hireDevSubs = this.hireService.hireDev$(this.hired).subscribe({
      next: () => {
        this.openSnackBar('successfully hired');
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(hireDevSubs);
  }

  // developer selected to team and collect dev dates
  // devSelectedForJob event handler for child component (developer)
  devSelected(devAndTech: { dev: Developer; tech: Tech }) {
    // developer selected to team
    this.selectedDevelopers.push(devAndTech);

    // collect dates from all selected devs
    this.hiredDatesFromAllDevs = this.selectDates();
  }

  // developer deselected from team
  //  devDeSelectedForJob event handler for child component (developer)
  devDeSelected(devId: number) {
    this.selectedDevelopers = this.selectedDevelopers.filter(
      (devAndTech) => devAndTech.dev.id !== devId
    );
    this.hiredDatesFromAllDevs = this.selectDates();
  }

  // get dates from all devs
  selectDates(): { startDate: Date; endDate: Date }[] {
    let allDatesArr: { startDate: Date; endDate: Date }[] = [];
    if (this.selectedDevelopers.length > 0) {
      this.selectedDevelopers.forEach((d) => {
        allDatesArr.push(...d.dev.hiredDates);
      });
    }
    return allDatesArr;
  }

  // filter dates in mat-date-range-picker
  allDevsDateFilter = (d: Date | null): boolean => {
    const calDate = d || new Date();

    // array of Obects with start and end dates
    if (this.hiredDatesFromAllDevs) {
      for (let i = 0; i < this.hiredDatesFromAllDevs.length; i++) {
        let start = new Date(this.hiredDatesFromAllDevs[i].startDate);
        let end = new Date(this.hiredDatesFromAllDevs[i].endDate);

        if (calDate.getTime() >= start.getTime() && calDate <= end) {
          return false;
        }
      }
    }

    return true;
  };

  // hire team
  submitHireAll(newDates: { startDate: Date; endDate: Date }) {
    this.selectedDevelopers.forEach((d) => {
      //new dates for every developer in team
      d.dev.hiredDates.push(newDates);
      let hiredDates = d.dev.hiredDates;
      let body = { hiredDates, selectedForJob: false };

      this.devService.editDev$(body, d.dev.id).subscribe((d) => {
        // push to hired dates
        this.hired.push({ id: d.id, dates: newDates });
        // patch hired array on DB
        this.uploadHired();
        d.isHired = true;
        // delete from componetn arr
        this.developers = this.developers.filter((dev) => d.id !== dev.id);
        // load dev to arr. Updated in DB from developer component
        this.developers.push(d);
        this.resort();
      });
    });

    this.resort();
    // clear selection and emty selected
    this.selectedDevelopers = [];
  }

  // filter and search
  filterAndSearch(params: {
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  }) {
    this.page = 1;
    this.paginator.firstPage();
    if (this.pageEvent) {
      this.pageEvent.pageIndex = this.page;
    }
    this.params = params;
    this.loadWithParams(params);
  }

  loadWithParams(params: {
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  }) {
    let filterUrl = this.devService.searchUrl(params);
    if (filterUrl) {
      let loadDevsParams = this.devService
        .getHeaderTest$(this.page, this.limit, filterUrl)
        .subscribe({
          next: (response) => {
            let count = response.headers.get('X-Total-Count');
            if (count) {
              this.devsCount = +count;
            }
            this.developers = response.body;

            this.developers.forEach((d) => {
              this.devService.clearDevOldJobs(d);
              if (this.devService.checkDevHired(d)) {
                d.isHired = true;
              } else {
                d.isHired = false;
              }
            });
          },
          error: (err) => {
            if ((err = 'Missing authorization header')) {
              this.router.navigate(['/auth/login']);
            } else {
              console.log(err);
            }
          },
        });

      this.subscriptions.push(loadDevsParams);
    } else {
      this.loadDevs(this.page, this.limit);
    }
  }

  // on paginate event
  onPaginateChange(event: PageEvent) {
    this.limit = event.pageSize;
    localStorage.setItem('limit', '' + this.limit);
    this.page = event.pageIndex + 1;
    // this.developers = [];
    if (this.params) {
      this.loadWithParams(this.params);
    } else {
      this.loadDevs(this.page, this.limit);
    }
  }

  // cancel selected team
  cancelTeamSelection() {
    this.selectedDevelopers.forEach((devAndTech) => {
      devAndTech.dev.selectedForJob = false;
      let body = { selectedForJob: false };
      let editSubscr = this.devService
        .editDev$(body, devAndTech.dev.id)
        .subscribe();
      this.subscriptions.push(editSubscr);
    });
    this.selectedDevelopers = [];
    this.hiredDatesFromAllDevs = [];
  }

  // remove dev from team
  removeSelected(unselectedDev: Developer) {
    unselectedDev.selectedForJob = false;
    this.developers.forEach((d) => {
      if (d.id === unselectedDev.id) {
        d.selectedForJob = false;
      }
    });

    this.selectedDevelopers = this.selectedDevelopers.filter((dev) => {
      return dev.dev.id !== unselectedDev.id;
    });

    let body = { selectedForJob: false };
    let editSubscr = this.devService
      .editDev$(body, unselectedDev.id)
      .subscribe();
    this.subscriptions.push(editSubscr);
  }

  // default sort by create or edit
  sortByTime(devs: Developer[]) {
    let sorted = [...devs];
    sorted.sort((a, b) => {
      return new Date(b.time).getTime() - new Date(a.time).getTime();
    });
    return sorted;
  }

  sortByPriceAsc(devs: Developer[]) {
    let sorted = [...devs];
    sorted.sort((a, b) => {
      return a.pricePerHour - b.pricePerHour;
    });
    return sorted;
  }
  sortByPriceDesc(devs: Developer[]) {
    let sorted = [...devs];
    sorted.sort((a, b) => {
      return b.pricePerHour - a.pricePerHour;
    });
    return sorted;
  }

  resort() {
    // console.log(this.params?.sortDirection);

    if (!this.params) {
      this.developers = this.sortByTime(this.developers);
    } else if (this.params && this.params.sortDirection === 'asc') {
      this.developers = this.sortByPriceAsc(this.developers);
    } else if (this.params && this.params.sortDirection === 'desc') {
      this.developers = this.sortByPriceDesc(this.developers);
    }
  }

  // snackBarMessage
  openSnackBar(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
      panelClass: ['dev-snackbar'],
    });
  }
}
