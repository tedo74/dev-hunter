import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { Developer } from 'src/app/models/dev.model';

@Injectable({
  providedIn: 'root',
})
export class DevsService {
  devsUrl = `http://localhost:3000/devs`;

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {}

  getUserId(): string {
    return this.authService.getUserId();
  }

  getAll$(params: string = ''): Observable<Developer[]> {
    let url = this.devsUrl;
    if (params) {
      url = this.devsUrl + params;
    }
    return this.httpClient.get<Developer[]>(url).pipe(
      map((devs) => {
        devs.forEach((d) => {
          this.clearDevOldJobs(d);
          if (this.checkDevHired(d)) {
            d.isHired = true;
          } else {
            d.isHired = false;
          }
        });
        return devs;
      })
    );
  }

  postDev$(f: FormGroup): Observable<Developer> {
    let userId = this.getUserId();
    let body = {
      sub: userId,
      ...f.value,
      hiredDates: [],
    };
    return this.httpClient.post<Developer>(this.devsUrl, body);
  }

  deleteDev$(devID: number): Observable<Developer> {
    return this.httpClient.delete<Developer>(`${this.devsUrl}/${devID}`);
  }

  getDev$(devID: number): Observable<Developer> {
    return this.httpClient.get<Developer>(`${this.devsUrl}/${devID}`);
  }

  editDev$(body: Object, devID: number): Observable<Developer> {
    let userId = this.getUserId();
    let bodyAdd = { ...body };
    return this.httpClient.patch<Developer>(
      `${this.devsUrl}/${devID}`,
      bodyAdd
    );
  }

  // chek is dev hired
  checkDevHired(dev: Developer): boolean {
    if (dev.hiredDates.length > 0) {
      return true;
    }
    return false;
  }

  // clear old jobs by date
  clearDevOldJobs(dev: Developer): void {
    if (this.checkDevHired(dev)) {
      let now = new Date();
      let clearedDates = [];
      for (let i = 0; i < dev.hiredDates.length; i++) {
        let jobEndDate = new Date(dev.hiredDates[i].endDate);
        if (jobEndDate >= now) {
          clearedDates.push(dev.hiredDates[i]);
        }
      }
      dev.hiredDates = clearedDates;
    }
  }

  // filter dates for mat-range-picker
  dateFilter(
    devDates: { startDate: Date; endDate: Date }[],
    calDate: Date
  ): boolean {
    for (let i = 0; i < devDates.length; i++) {
      let start = new Date(devDates[i].startDate);
      let end = new Date(devDates[i].endDate);

      if (calDate.getTime() >= start.getTime() && calDate <= end) {
        return false;
      }
    }
    return true;
  }

  searchUrl(params: {
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  }): string {
    let filterUrl = ``;
    if (params.techId !== 0) {
      filterUrl += `technology=${params.techId}`;
    }
    if (params.locationId !== 0) {
      if (filterUrl) {
        filterUrl += '&';
      }
      filterUrl += `location=${params.locationId}`;
    }
    if (params.searchTerm) {
      if (filterUrl) {
        filterUrl += '&';
      }
      filterUrl += `name_like=${params.searchTerm.trim().toUpperCase()}`;
    }
    if (params.sortDirection) {
      if (filterUrl) {
        filterUrl += '&';
      }
      filterUrl += `_sort=pricePerHour&_order=${params.sortDirection}`;
    }
    if (filterUrl) {
      filterUrl = '?' + filterUrl;
    }
    return filterUrl;
  }

  getHeaderTest$(page: number = 1, limit: number = 2, params: string = '') {
    let url = `http://localhost:3000/devs`;
    if (params) {
      url = url + params;
      url += `&_page=${page}&_limit=${limit}`;
    } else {
      let defaultSortByTime = `?_sort=time&_order=desc`;
      url += `${defaultSortByTime}&_page=${page}&_limit=${limit}`;
    }
    return this.httpClient.get<any>(url, {
      observe: 'response',
    });
  }
}
