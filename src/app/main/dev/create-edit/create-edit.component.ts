import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { Developer } from 'src/app/models/dev.model';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { Tech } from 'src/app/models/tech.model';
import { HunterService } from '../../hunter/hunter.service';
import { DevsService } from '../devs.service';

@Component({
  selector: 'dev-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss'],
})
export class CreateEditComponent implements OnInit, OnDestroy {
  // false - create, true - edit
  hasParamsId = false;
  locations: DevLocation[] = [];
  technologies: Tech[] = [];
  devId: string | null = null;
  currentDev!: Developer;
  buttonName = 'Create';

  // on Edit compare old and new to update if needed
  currentDevOldLocationId!: number;
  currentDevOldTechId!: number;

  subscriptions: Subscription[] = [];
  // remove flicker ngIf show component
  haveLocations = false;
  haveTechnologies = false;

  // html selected option
  selectedLoc = 0;
  selectedTech = 0;

  devForm: FormGroup;
  constructor(
    private devService: DevsService,
    private hunterServ: HunterService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.devForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(2)]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.pattern('^[A-Za-z0-9._]+@[A-Za-z0-9._]+\\.[a-z]{2,3}$'),
      ]),
      phone: new FormControl('', [Validators.required]),
      location: new FormControl('', [Validators.required]),
      technology: new FormControl('', [Validators.required]),
      pricePerHour: new FormControl('', [Validators.required]),
      yearsExperience: new FormControl('', [Validators.required]),
      language: new FormControl('', [Validators.required]),
      pictureUrl: new FormControl(''),
      linkedinLink: new FormControl(''),
      description: new FormControl(''),
    });
  }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.get('id') !== null) {
      this.devId = this.route.snapshot.paramMap.get('id');
      this.hasParamsId = true;
      // edit
      if (this.devId) {
        this.buttonName = 'Edit';
        let id = +this.devId;
        let getDevSubs = this.devService
          .getDev$(id)
          .subscribe((d: Developer) => {
            // get dev and save dev location and dev tech
            this.currentDev = d;
            this.currentDevOldLocationId = d.location;
            this.currentDevOldTechId = d.technology;
            this.getLocations();
          });

        this.subscriptions.push(getDevSubs);
      }
    } else {
      // create
      this.getLocations();
      this.getTechnologies();
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  onEdit(newLoc: number, oldLoc: number, newTech: number, oldTech: number) {
    if (oldLoc !== newLoc) {
      // decrement old loc
      this.updateLocUsed(oldLoc, false).subscribe();
      // increment new loc
      this.updateLocUsed(newLoc, true).subscribe();
    }
    if (oldTech !== newTech) {
      // decrement old tech
      this.updateTechUsed(oldTech, false).subscribe();
      // increment new tech
      this.updateTechUsed(newTech, true).subscribe();
    }
  }

  onCreateHandler() {
    // gets only location id to save in new dev
    let f = this.devForm;
    let locId = +f.value.location.id;
    let techId = +f.value.technology.id;
    f.value.name = f.value.name.trim().toUpperCase();

    f.value.location = locId;

    f.value.technology = techId;
    f.value.time = new Date();

    // on edit
    if (this.devId && this.currentDev) {
      this.onEdit(
        locId,
        this.currentDevOldLocationId,
        techId,
        this.currentDevOldTechId
      );
      let editDevSubs = this.devService
        .editDev$(f.value, this.currentDev.id)
        .subscribe({
          next: () => {
            this.router.navigate(['/devs']);
          },
          error: (err) => {
            if ((err = 'Missing authorization header')) {
              this.router.navigate(['/auth/login']);
            } else {
              console.log(err);
            }
          },
        });

      this.subscriptions.push(editDevSubs);
    } else {
      // on create
      let postSub = this.devService.postDev$(f).subscribe({
        next: () => {
          this.updateLocUsed(locId, true).subscribe();
          this.updateTechUsed(techId, true).subscribe();

          this.router.navigate(['/devs']);
          // this.subscriptions.push(updateLoc, updateTech);
        },
        error: (err) => {
          if ((err = 'Missing authorization header')) {
            this.router.navigate(['/auth/login']);
          } else {
            console.log(err);
          }
        },
      });

      this.subscriptions.push(postSub);
    }
  }

  // get locations
  getLocations() {
    let locSub = this.hunterServ.getAllLocations$().subscribe({
      next: (loc) => {
        this.locations = loc;
        this.haveLocations = true;

        // on edit
        if (this.hasParamsId) {
          this.setLocationIndex();
          this.getTechnologies();
        }
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(locSub);
  }
  // get techs
  getTechnologies() {
    let techSub = this.hunterServ.getAllTechs$().subscribe({
      next: (techs) => {
        this.technologies = techs;
        this.haveTechnologies = true;
        if (this.hasParamsId) {
          this.setTechIndex();
          this.setValues();
        }
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(techSub);
  }

  // go and create location or tech first
  toHunter() {
    this.router.navigate(['/hunter']);
  }

  // increment or decrement location used
  updateLocUsed(id: number, add: boolean): Observable<DevLocation> {
    return this.hunterServ.getLocation$(id).pipe(
      mergeMap((old) => {
        let locUsed = +old.used;
        if (add) {
          locUsed++;
        } else {
          locUsed--;
        }
        let body = { used: locUsed };
        return this.hunterServ.updateLocation$(id, body);
      })
    );
  }

  // increment or decrement technology used
  updateTechUsed(id: number, add: boolean): Observable<Tech> {
    return this.hunterServ.getTech$(id).pipe(
      mergeMap((old) => {
        let techUsed = +old.used;
        if (add) {
          techUsed++;
        } else {
          techUsed--;
        }
        let body = { used: techUsed };
        return this.hunterServ.updateTech$(id, body);
      })
    );
  }

  setLocationIndex() {
    if (this.locations && this.locations.length > 0) {
      let devLocation = this.currentDev.location;
      for (let i = 0; i < this.locations.length; i++) {
        if (devLocation === this.locations[i].id) {
          this.selectedLoc = i;
          break;
        }
      }
    }
  }

  setTechIndex() {
    if (this.technologies && this.technologies.length > 0) {
      let devTech = this.currentDev.technology;
      for (let i = 0; i < this.technologies.length; i++) {
        if (devTech === this.technologies[i].id) {
          this.selectedTech = i;
          break;
        }
      }
    }
  }

  // setFormValuesOnEdit
  setValues() {
    if (this.currentDev && this.locations && this.technologies) {
      this.devForm.setValue({
        name: this.currentDev.name,
        email: this.currentDev.email,
        phone: this.currentDev.phone,
        location: this.locations[this.selectedLoc],
        technology: this.technologies[this.selectedTech],
        pricePerHour: this.currentDev.pricePerHour,
        yearsExperience: this.currentDev.yearsExperience,
        language: this.currentDev.language,
        pictureUrl: this.currentDev.pictureUrl,
        linkedinLink: this.currentDev.linkedinLink,
        description: this.currentDev.description,
      });
    }
  }

  // cancel - redirect to devs
  cancelForm() {
    this.router.navigate(['/devs']);
  }
}
