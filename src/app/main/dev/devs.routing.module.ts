import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllDevsComponent } from './all-devs/all-devs.component';
import { CreateEditComponent } from './create-edit/create-edit.component';

const routes: Routes = [
  { path: '', component: AllDevsComponent },
  // { path: 'create', component: CreateDevComponent },
  // { path: 'edit/:id', component: EditDevComponent },
  { path: 'create', component: CreateEditComponent },
  { path: 'edit/:id', component: CreateEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DevsRoutingModule {}
