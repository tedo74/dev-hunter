import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'dev-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit, OnDestroy {
  isAuth = this.authService.isUserAuth();
  name = '';

  subscriptions: Subscription[] = [];

  constructor(private authService: AuthService) {
    if (this.authService.isUserAuth()) {
      this.getName();
    }
  }

  ngOnInit(): void {
    let authSubscription = this.authService.authChange.subscribe(
      (authStatus) => {
        this.isAuth = authStatus;
        if (this.isAuth) {
          this.getName();
        } else {
          this.name = '';
        }
      }
    );
    this.subscriptions.push(authSubscription);
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }
  logOut(): void {
    this.authService.logOut();
  }
  // get user name to show on nav
  getName(): void {
    let getNameSubs = this.authService.getName$().subscribe((name: string) => {
      this.name = name;
    });
    this.subscriptions.push(getNameSubs);
  }
}
