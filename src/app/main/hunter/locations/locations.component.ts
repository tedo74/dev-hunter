import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, Subscription } from 'rxjs';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { HunterService } from '../hunter.service';

@Component({
  selector: 'dev-locations',
  templateUrl: './locations.component.html',
  styleUrls: ['./locations.component.scss'],
})
export class LocationsComponent implements OnInit, OnDestroy {
  locations: DevLocation[] = [];
  locExists = '';
  formInput = '';

  // used in child component to edit
  locationToEdit!: DevLocation;

  subscriptions: Subscription[] = [];

  constructor(private hunterServ: HunterService, private router: Router) {}

  ngOnInit(): void {
    this.loadLocations();
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  loadLocations() {
    let loadLocsSubs = this.hunterServ.getAllLocations$().subscribe({
      next: (locations) => {
        this.locations = locations;
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(loadLocsSubs);
  }

  remove(loc: DevLocation) {
    let delSubs = this.hunterServ.deleteLocation$(loc.id).subscribe({
      next: () => {
        this.loadLocations();
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(delSubs);
  }

  // edit current location
  edit(loc: DevLocation) {
    this.locationToEdit = loc;
    this.hunterServ.LocationToEditChanged.next(loc);
  }

  // on edit
  onEditLocationHandler(f: FormGroup) {
    // update view
    this.locationToEdit.location = f.value.location.trim().toUpperCase();
    this.locationToEdit.link = f.value.link;
    // hide message
    this.locExists = '';
    // location name to uppercase
    let inputLoc = f.value.location.trim().toUpperCase();
    f.value.location = inputLoc;
    // get id of current location
    let locId = this.locationToEdit.id;
    let editSubs = this.hunterServ.updateLocation$(locId, f.value).subscribe();

    this.subscriptions.push(editSubs);
  }

  // check if location is used. If not removable = true
  isLocRemovable(location: DevLocation): boolean {
    if (location.used > 0) {
      location.removable = false;
      return location.removable;
    }
    location.removable = true;
    return location.removable;
  }

  // compare new location to all locations
  checkLocationExist(loc: string): boolean {
    for (let i = 0; i < this.locations.length; i++) {
      const element = this.locations[i];
      if (element.location.toUpperCase() === loc.toUpperCase()) {
        return true;
      }
    }
    return false;
  }

  onCreateLocationHandler(f: FormGroup) {
    this.locExists = '';
    this.loadLocations();

    let inputLoc = f.value.location.trim().toUpperCase();
    if (this.checkLocationExist(inputLoc)) {
      this.locExists = 'This location already exists.';
      return;
    }
    let postLocSubs = this.hunterServ.postLoc$(f).subscribe({
      next: (d: DevLocation) => {
        this.loadLocations();
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(postLocSubs);
  }
  // clear
  onCancel() {
    this.locExists = '';
  }
}
