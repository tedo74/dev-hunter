import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { HunterService } from '../../hunter.service';

@Component({
  selector: 'dev-location-create-edit',
  templateUrl: './location-create-edit.component.html',
  styleUrls: ['./location-create-edit.component.scss'],
})
export class LocationCreateEditComponent implements OnInit, OnDestroy {
  location: DevLocation | null = null;
  locationSubscr!: Subscription;
  @Output() createLocEvent = new EventEmitter<FormGroup>();
  @Output() editLocEvent = new EventEmitter<FormGroup>();
  // clear message in parent component
  @Output() cancel = new EventEmitter();
  buttonName = 'Create';

  locationEditForm: FormGroup;
  @ViewChild('formDirective') private formDirective!: FormGroupDirective;
  constructor(private hunterService: HunterService, private router: Router) {
    this.locationEditForm = new FormGroup({
      location: new FormControl('', [
        Validators.required,
        Validators.minLength(2),
      ]),
      link: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.locationSubscr = this.hunterService.LocationToEditChanged.subscribe({
      next: (location) => {
        this.location = location;
        if (this.location) {
          this.buttonName = 'Edit';
          this.locationEditForm.setValue({
            location: this.location.location,
            link: this.location.link,
          });
        } else {
          this.buttonName = 'Create';
        }
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
  }

  ngOnDestroy(): void {
    if (this.locationSubscr) {
      this.locationSubscr.unsubscribe();
    }
  }

  onEdit() {
    if (this.location !== null) {
      this.editLocEvent.emit(this.locationEditForm);
    } else {
      this.createLocEvent.emit(this.locationEditForm);
    }
    this.location = null;
    this.locationEditForm.reset();
    this.formDirective.resetForm();
  }

  onCancel() {
    this.location = null;
    this.buttonName = 'Create';
    this.locationEditForm.reset();
    this.formDirective.resetForm();
    this.cancel.emit();
  }
}
