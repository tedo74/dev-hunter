import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Developer } from 'src/app/models/dev.model';
import { Hunter } from 'src/app/models/huter.model';
import { DevsService } from '../../dev/devs.service';
import { HireService } from '../../hire.service';

@Component({
  selector: 'dev-hired',
  templateUrl: './hired.component.html',
  styleUrls: ['./hired.component.scss'],
})
export class HiredComponent implements OnInit, OnDestroy {
  // search / filter params
  params: {
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  } | null = null;

  // all hired devs id hire
  hired: Hunter[] = [];

  // developers and job dates
  hiredDevelopers: {
    developer: Developer;
    dates: { startDate: Date; endDate: Date };
  }[] = [];

  // filter and search
  filteredHiredDevelopers: {
    developer: Developer;
    dates: { startDate: Date; endDate: Date };
  }[] = [];
  currentDev!: Developer;

  // colect all subscriptions to unsubscribe;
  subscriptions: Subscription[] = [];

  constructor(
    private hireServ: HireService,
    private devService: DevsService,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getHired();
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  getHired() {
    let getHiredSubs = this.hireServ.getHired$().subscribe({
      next: (d) => {
        this.hired = d;
        this.getHiredDevs();
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(getHiredSubs);
  }

  // get id ,load developer and pack with dates
  getHiredDevs() {
    if (this.hired.length > 0) {
      // for evry ID in hired load developer with This ID and put dates
      for (let i = 0; i < this.hired.length; i++) {
        let startDate = new Date(this.hired[i].dates.startDate);
        let endDate = new Date(this.hired[i].dates.endDate);

        let getDevSubs = this.devService.getDev$(this.hired[i].id).subscribe({
          next: (d: Developer) => {
            this.hiredDevelopers.push({
              developer: d,
              dates: { startDate, endDate },
            });
            this.filteredHiredDevelopers.push({
              developer: d,
              dates: { startDate, endDate },
            });
          },
          error: (err) => {
            if ((err = 'Missing authorization header')) {
              this.router.navigate(['/auth/login']);
            } else {
              console.log(err);
            }
          },
        });
        this.subscriptions.push(getDevSubs);
      }
    }
  }

  // remove dev from hired for certain period of time (start - end)
  removeFromProj(devAndDates: {
    developer: Developer;
    dates: { startDate: Date; endDate: Date };
  }) {
    // selected dev
    let developer = devAndDates.developer;

    // selected dev dates
    let dates = devAndDates.dates;

    // get dev from DB by id
    let getDevSubs = this.devService.getDev$(developer.id).subscribe(
      (d) => {
        this.currentDev = d;
        // delete dates
        this.currentDev.hiredDates = this.currentDev.hiredDates.filter((d) => {
          return (
            new Date(d.startDate).getTime() !==
              new Date(dates.startDate).getTime() ||
            new Date(d.endDate).getTime() !== new Date(dates.endDate).getTime()
          );
        });
        // save dev to DB with updated dates
        let editDevSubs = this.devService
          .editDev$(
            { hiredDates: [...this.currentDev.hiredDates] },
            developer.id
          )
          .subscribe((d) => {
            // clear from container
            this.hiredDevelopers = this.hiredDevelopers.filter((d) => {
              if (devAndDates.developer.id !== d.developer.id) {
                return true;
              } else if (
                new Date(devAndDates.dates.startDate).getTime() !==
                  new Date(d.dates.startDate).getTime() ||
                new Date(devAndDates.dates.endDate).getTime() !==
                  new Date(d.dates.endDate).getTime()
              ) {
                return true;
              } else {
                return false;
              }
            });
            this.filteredHiredDevelopers = this.hiredDevelopers;
            if (this.params) {
              this.filterHired(this.params);
            }

            // clear from array with devs id
            this.hired = this.hired.filter((d) => {
              if (d.id !== this.currentDev.id) {
                return true;
              }
              return (
                new Date(d.dates.startDate).getTime() !==
                  new Date(dates.startDate).getTime() ||
                new Date(d.dates.endDate).getTime() !==
                  new Date(dates.endDate).getTime()
              );
            });
            let hireSubs = this.hireServ.hireDev$(this.hired).subscribe();
            this.subscriptions.push(hireSubs);
            this.openSnackBar('developer removed');
          });
        this.subscriptions.push(editDevSubs);
      },
      (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      }
    );

    this.subscriptions.push(getDevSubs);
  }

  // filter and search
  filterAndSearch(params: {
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  }) {
    this.params = params;
    this.filterHired(params);
  }

  filterHired(params: {
    locationId: number;
    techId: number;
    searchTerm: string;
    sortDirection: string;
  }) {
    let devs = [...this.hiredDevelopers];

    if (params.locationId) {
      devs = devs.filter((d) => {
        return d.developer.location === params.locationId;
      });
    }

    if (params.techId) {
      devs = devs.filter((hd) => {
        return hd.developer.technology === params.techId;
      });
    }
    if (params.searchTerm) {
      devs = devs.filter((hd) => {
        return hd.developer.name.includes(
          params.searchTerm.trim().toUpperCase()
        );
      });
    }
    if (params.sortDirection) {
      if (params.sortDirection === 'asc') {
        devs = devs.sort(
          (a, b) => a.developer.pricePerHour - b.developer.pricePerHour
        );
      } else {
        devs = devs.sort(
          (a, b) => b.developer.pricePerHour - a.developer.pricePerHour
        );
      }
    }
    this.filteredHiredDevelopers = [...devs];
  }

  // snackBarMessage
  openSnackBar(message: string) {
    this.snackBar.open(message, 'Close', {
      duration: 2000,
      panelClass: ['dev-snackbar'],
    });
  }
}
