import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HunterRoutingModule } from './hunter-routing.module';
import { HunterAllComponent } from './hunter-all/hunter-all.component';
import { LocationsComponent } from './locations/locations.component';
import { MaterialModule } from 'src/app/material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HiredComponent } from './hired/hired.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { TechnologiesComponent } from './technologies/technologies.component';
import { LocationCreateEditComponent } from './locations/location-create-edit/location-create-edit.component';
import { TechCreateEditComponent } from './technologies/tech-create-edit/tech-create-edit.component';

@NgModule({
  declarations: [
    HunterAllComponent,
    LocationsComponent,
    HiredComponent,
    TechnologiesComponent,
    LocationCreateEditComponent,
    TechCreateEditComponent,
  ],
  imports: [
    CommonModule,
    HunterRoutingModule,
    MaterialModule,
    FormsModule,
    SharedModule,
    ReactiveFormsModule,
  ],
})
export class HunterModule {}
