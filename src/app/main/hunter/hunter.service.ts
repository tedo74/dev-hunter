import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Observable, Subject } from 'rxjs';
import { AuthService } from 'src/app/auth/auth.service';
import { DevLocation } from 'src/app/models/devLocationsmodel';
import { Tech } from 'src/app/models/tech.model';

@Injectable({
  providedIn: 'root',
})
export class HunterService {
  locationsUrl = `http://localhost:3000/locations/`;
  techsUrl = `http://localhost:3000/tech/`;
  locationToEdit!: DevLocation;
  technologyToEdit!: Tech;
  LocationToEditChanged = new Subject<DevLocation | null>();
  TechnologyToEditChanged = new Subject<Tech | null>();
  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {}

  getUserId(): string {
    return this.authService.getUserId();
  }

  getAllLocations$(): Observable<DevLocation[]> {
    return this.httpClient.get<DevLocation[]>(this.locationsUrl);
  }

  getAllTechs$(): Observable<Tech[]> {
    return this.httpClient.get<Tech[]>(this.techsUrl);
  }

  getLocation$(id: number): Observable<DevLocation> {
    return this.httpClient.get<DevLocation>(`${this.locationsUrl}${id}`);
  }
  getTech$(id: number): Observable<Tech> {
    return this.httpClient.get<Tech>(`${this.techsUrl}${id}`);
  }

  postLoc$(f: FormGroup): Observable<DevLocation> {
    let userId = this.getUserId();
    let body = {
      sub: userId,
      location: f.value.location.trim().toUpperCase(),
      link: f.value.link,
      used: 0,
      removable: true,
    };

    return this.httpClient.post<DevLocation>(this.locationsUrl, body);
  }

  postTech$(f: FormGroup): Observable<Tech> {
    let userId = this.getUserId();
    let body = {
      sub: userId,
      techName: f.value.techName.trim().toUpperCase(),
      imageUrl: f.value.imageUrl,
      used: 0,
      removable: true,
    };
    return this.httpClient.post<Tech>(this.techsUrl, body);
  }

  deleteLocation$(locID: number): Observable<DevLocation> {
    return this.httpClient.delete<DevLocation>(`${this.locationsUrl}${locID}`);
  }

  deleteTech$(techId: number): Observable<Tech> {
    return this.httpClient.delete<Tech>(`${this.techsUrl}${techId}`);
  }

  // update location used
  updateLocation$(locID: number, data: Object): Observable<DevLocation> {
    let body = {
      ...data,
    };
    return this.httpClient.patch<DevLocation>(
      `${this.locationsUrl}${locID}`,
      body
    );
  }

  // update tech used
  updateTech$(techId: number, data: Object): Observable<Tech> {
    let body = {
      ...data,
    };
    return this.httpClient.patch<Tech>(`${this.techsUrl}${techId}`, body);
  }
}
