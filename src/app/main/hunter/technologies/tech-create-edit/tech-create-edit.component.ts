import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Tech } from 'src/app/models/tech.model';
import { HunterService } from '../../hunter.service';

@Component({
  selector: 'dev-create-edit-tech',
  templateUrl: './tech-create-edit.component.html',
  styleUrls: ['./tech-create-edit.component.scss'],
})
export class TechCreateEditComponent implements OnInit {
  technology: Tech | null = null;
  techSubscr!: Subscription;
  @Output() createTechEvent = new EventEmitter<FormGroup>();
  @Output() editTechEvent = new EventEmitter<FormGroup>();
  // clear message in parent component
  @Output() cancel = new EventEmitter();
  buttonName = 'Create';

  techForm: FormGroup;
  @ViewChild('formDirective') private formDirective!: FormGroupDirective;
  constructor(private hunterService: HunterService, private router: Router) {
    this.techForm = new FormGroup({
      techName: new FormControl('', [Validators.required]),
      imageUrl: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.techSubscr = this.hunterService.TechnologyToEditChanged.subscribe({
      next: (technology) => {
        this.technology = technology;
        if (this.technology) {
          this.buttonName = 'Edit';
          this.techForm.setValue({
            techName: this.technology.techName,
            imageUrl: this.technology.imageUrl,
          });
        } else {
          this.buttonName = 'Create';
        }
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
  }

  ngOnDestroy(): void {
    if (this.techSubscr) {
      this.techSubscr.unsubscribe();
    }
  }

  onEdit() {
    if (this.technology !== null) {
      this.editTechEvent.emit(this.techForm);
    } else {
      this.createTechEvent.emit(this.techForm);
    }
    this.technology = null;
    this.techForm.reset();
    this.formDirective.resetForm();
  }

  onCancel() {
    this.technology = null;
    this.buttonName = 'Create';
    this.techForm.reset();
    this.formDirective.resetForm();
    this.cancel.emit();
  }
}
