import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Tech } from 'src/app/models/tech.model';
import { HunterService } from '../hunter.service';

@Component({
  selector: 'dev-technologies',
  templateUrl: './technologies.component.html',
  styleUrls: ['./technologies.component.scss'],
})
export class TechnologiesComponent implements OnInit, OnDestroy {
  techs: Tech[] = [];
  techExists = '';

  subscriptions: Subscription[] = [];

  // switch create and edit component true=create component
  showCreate = true;

  // used in child component to edit
  techToEdit!: Tech;

  constructor(private hunterServ: HunterService, private router: Router) {}

  ngOnInit(): void {
    this.loadTechs();
  }
  ngOnDestroy(): void {
    this.subscriptions.forEach((subscrption) => {
      if (subscrption) {
        subscrption.unsubscribe();
      }
    });
  }

  // load all techs
  loadTechs() {
    let techsSubscription = this.hunterServ.getAllTechs$().subscribe({
      next: (techs) => {
        this.techs = techs;
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(techsSubscription);
  }

  // remove tech
  remove(tech: Tech) {
    let deleteTech = this.hunterServ.deleteTech$(tech.id).subscribe({
      next: () => {
        this.loadTechs();
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });
    this.subscriptions.push(deleteTech);
  }

  // edit current technology
  edit(tech: Tech) {
    this.techToEdit = tech;
    this.hunterServ.TechnologyToEditChanged.next(tech);

    // switch to edit component
    this.showCreate = false;
  }

  onEditTechHandler(f: FormGroup) {
    this.techToEdit.techName = f.value.techName.trim().toUpperCase();
    this.techToEdit.imageUrl = f.value.imageUrl;
    this.techExists = '';
    // tech name to uppercase
    let inputTech = f.value.techName.trim().toUpperCase();
    f.value.techName = inputTech;

    // id of current tech
    let techId = this.techToEdit.id;
    let updateTech = this.hunterServ.updateTech$(techId, f.value).subscribe();
    this.subscriptions.push(updateTech);
  }

  // check if tech is used. If not removable = true
  isTechRemovable(tech: Tech): boolean {
    if (tech.used > 0) {
      tech.removable = false;
      return tech.removable;
    }
    tech.removable = true;
    return tech.removable;
  }

  // compare new tech to all technologies
  checkTechExist(loc: string): boolean {
    for (let i = 0; i < this.techs.length; i++) {
      const element = this.techs[i];
      if (element.techName.toUpperCase() === loc.toUpperCase()) {
        return true;
      }
    }
    return false;
  }

  onCreateTechHandler(f: FormGroup) {
    this.techExists = '';
    this.loadTechs();

    let inputTech = f.value.techName.trim().toUpperCase();
    if (this.checkTechExist(inputTech)) {
      this.techExists = 'This technology already exists.';
      return;
    }

    let postTech = this.hunterServ.postTech$(f).subscribe({
      next: () => {
        this.loadTechs();
        // f.reset();
      },
      error: (err) => {
        if ((err = 'Missing authorization header')) {
          this.router.navigate(['/auth/login']);
        } else {
          console.log(err);
        }
      },
    });

    this.subscriptions.push(postTech);
  }

  onCancel() {
    this.techExists = '';
  }
}
