import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HunterAllComponent } from './hunter-all/hunter-all.component';

const routes: Routes = [{ path: '', component: HunterAllComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HunterRoutingModule {}
